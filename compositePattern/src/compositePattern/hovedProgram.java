package compositePattern;

public class hovedProgram {
	public static void main(String[] args) {
		
		// gruppe med overleger
		helsePersonellGruppe overlegeGruppe = new helsePersonellGruppe();
		overlegeGruppe.leggTil(new overlege("Johnsen", 500000));
		overlegeGruppe.leggTil(new overlege("Olsen", 600000));
		
		//gruppe med sykepleiere
		helsePersonellGruppe sykepleierGruppe = new helsePersonellGruppe();
		sykepleierGruppe.leggTil(new sykepleier("Rita", 100000));
		sykepleierGruppe.leggTil(new sykepleier("Marit", 150000));
		
		//gruppe med medisinstudenter.
		helsePersonellGruppe studentGruppe = new helsePersonellGruppe();
		studentGruppe.leggTil(new medisinStudent("Oscar", 10000));
		studentGruppe.leggTil(new medisinStudent("Mai", 20000));
		
		overlegeGruppe.leggTil(sykepleierGruppe); //sykepleierGruppen legges til i overlegeGruppen.
		sykepleierGruppe.leggTil(studentGruppe);  //Medisingruppen legges til i sykepleiergruppen. 
		
		overlegeGruppe.visLonn();
	}
}
