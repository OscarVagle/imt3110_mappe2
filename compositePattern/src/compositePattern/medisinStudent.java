package compositePattern;

//bladnode
public class medisinStudent implements helsePersonell {

	String navn;
	int lonn;
	
	public medisinStudent(String navn, int lonn) {
		this.navn = navn;
		this.lonn = lonn;
	}
	
	@Override
	public void visLonn() {
		System.out.println(navn + " : medisinstudent har " + lonn + " i lonn.");
		
	}
	
	public void taImotOrdre() {
		System.out.println(navn + " tar imot ordre fra sykepleier.");
	}

}
