package compositePattern;


//bladnode
public class overlege implements helsePersonell {
	String navn;
	int lonn;
	
	public overlege(String navn, int lonn) {
		this.navn = navn;
		this.lonn = lonn;
	}
	
	public void deltaIMote() {
		System.out.println(navn + " deltar i et mote.");
	}

	@Override
	public void visLonn() {
		System.out.println(navn + " : overlege har " + lonn + " i lonn.");
		
	}
}
