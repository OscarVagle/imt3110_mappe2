package compositePattern;


//bladnode
public class sykepleier implements helsePersonell {
	String navn;
	int lonn;
	
	public sykepleier(String navn, int lonn) {
		this.navn = navn;
		this.lonn = lonn;
	}
	
	public void delUtMedisin() {
		System.out.println("Deler ut medisin [medisinnavn] til pasient [pasientnavn]");
	}

	@Override
	public void visLonn() {
		System.out.println(navn + " : sykepleier har " + lonn + " i lonn.");
		
	}
	
}
