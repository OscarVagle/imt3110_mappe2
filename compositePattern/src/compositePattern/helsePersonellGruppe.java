package compositePattern;

import java.util.*;


//Composite. Denne klassen kan inneholde en gruppe av bladnoder som sykepleiere, overleger og medisinstudenter. 
public class helsePersonellGruppe implements helsePersonell {

	List<helsePersonell> personellListe = new ArrayList<helsePersonell>();
	
	public helsePersonellGruppe() {
		
	}
	
	@Override
	public void visLonn() {
		for (helsePersonell personell : personellListe) {
			personell.visLonn();
		}
		
	}
	
	public void leggTil(helsePersonell personell) {
		personellListe.add(personell);
	}
	
}
