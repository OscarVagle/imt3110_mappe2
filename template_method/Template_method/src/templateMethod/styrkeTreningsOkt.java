package templateMethod;

public class styrkeTreningsOkt extends treningsOkt{
	
	int antPushUps;
	int antSitUps;
	
	public styrkeTreningsOkt(boolean deleOkt) {
		super(deleOkt);
	}
	
	@Override
	void giDataSkrivTilFil(String dataTilFil) {
		super.skrivTilFil(dataTilFil);
		
	}

	@Override
	void stillSpoermalene() {
		sted = super.stillSpoersmaal("Hvilket sted har du trent styrke?");
		antPushUps = super.stillSpoersmaalInt("Hvor mange push ups har du tatt?");
		antSitUps = super.stillSpoersmaalInt("Hvor mange sit ups har du tatt?");
		
		giDataSkrivTilFil("sted: " + sted + "\nant. push ups: " + antPushUps + "\nant. sit ups: " + antSitUps);
		
	}

	@Override
	void delTreningsOkt() {}

}
