package templateMethod;

public class joggeOkt extends treningsOkt{
	
	int km;
	int tid;
	float gjennomSnitt;  //gjennomsnittstid per kilometer
	
	public joggeOkt(boolean deleOkt) {
		super(deleOkt);
	}
	
	@Override
	void giDataSkrivTilFil(String dataTilFil) {
		super.skrivTilFil(dataTilFil);
		
	}

	@Override
	void stillSpoermalene() {
		sted = super.stillSpoersmaal("Hvilket sted har du lopt?");
		km = super.stillSpoersmaalInt("Hvor langt har du lopt? (km)");
		tid = super.stillSpoersmaalInt("Hvor lang tid brukt du paa aa lope strekningen? (i minutt)");
		gjennomSnitt = tid /km;
		
		giDataSkrivTilFil("sted: " + sted + "\nkm: " + km + "\ntid: " + tid + "\ngjennomsnitt: " + gjennomSnitt);
	}

	@Override
	void delTreningsOkt() {
		System.out.println("deler joggeturen paa Facebook for likes.");
		
	}
}
