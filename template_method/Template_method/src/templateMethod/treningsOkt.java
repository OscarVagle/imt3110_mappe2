package templateMethod;

import java.io.FileWriter;              //skrive til fil
import java.io.IOException;
import java.io.PrintWriter;
import java.io.BufferedWriter;

import java.util.Scanner;

//abstrakt klasse som inneholder skjelettet av programmet. 
//Funksjonene "giDataTilSkrivTilFil()" og "stillSpoermalene()" er abtrakte fordi de skal defineres i baseklassene.
public abstract class treningsOkt {
	String sted;
	boolean deleTreningsOkt;
	
	//Lager constructor som kan intialisere "deleTreningsOkt" siden subklassene skal bestemme om de skal benytte seg av
	//hooket eller ikke. 
	public treningsOkt(boolean deleOkt) {
		deleTreningsOkt = deleOkt;
	}
	
	final protected void skrivTilFil(String dataTilFil) {
		
		try {
			FileWriter fw = new FileWriter("treningsOkter.txt", true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter writer = new PrintWriter(bw);
			
			writer.println("\n" + dataTilFil);
			writer.close();
		} catch (IOException e) {
			System.out.println("error occurred");
		}
	}
	
	final protected String stillSpoersmaal(String spoersmal) {
		Scanner reader = new Scanner(System.in);
		
		System.out.println(spoersmal);
		return reader.nextLine();
	}
	
	final protected int stillSpoersmaalInt(String spoermal) {
		Scanner reader = new Scanner(System.in);
		
		System.out.println(spoermal);
		return reader.nextInt();
	}
	
	//Selve template method. Denne viser skjelettet av koden som blir kj�rt.
	final protected void startProsess() {
		stillSpoermalene();
		if (deleTreningsOkt) {              //Her er if-setningen for hooket. 
			delTreningsOkt();
		}
	}
	
	abstract void giDataSkrivTilFil(String dataTilFil);
	
	abstract void stillSpoermalene();   //funksjon som stiller alle spoersmalene for treningsokten. 
	
	//Hook. Dette er ekstra funksjonskall som valgfrie. Baseklassene kan velge � bruke den eller ikke. 
	abstract void delTreningsOkt();
}
