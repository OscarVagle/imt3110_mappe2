package templateMethod;

public class svommeOkt extends treningsOkt{
	int meter;
	int tid;
	float gjennomSnitt;  //gjennomsnittstid per meter.
	
	public svommeOkt(boolean deleOkt) {
		super(deleOkt);
	}
	
	@Override
	void giDataSkrivTilFil(String dataTilFil) {
		super.skrivTilFil(dataTilFil);
		
	}

	@Override
	void stillSpoermalene() {
		sted = super.stillSpoersmaal("Hvilket sted har du svomt?");
		meter = super.stillSpoersmaalInt("Hvor langt har du svomt? (meter)");
		tid = super.stillSpoersmaalInt("Hvor lang tid brukt du paa aa svomme strekningen? (i minutt)");
		gjennomSnitt = tid / meter;
		
		giDataSkrivTilFil("sted: " + sted + "\nmeter: " + meter + "\ntid: " + tid + "\ngjennomsnitt: " + gjennomSnitt);
		
	}

	@Override
	void delTreningsOkt() {}
}
