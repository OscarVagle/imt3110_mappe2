package templateMethod;

import java.util.Scanner;

public class hovedProgram {
	public static void main(String[] args) {
		Scanner leseInput = new Scanner(System.in);
		int valg = 0;
		
		System.out.println("Har du tatt en joggetur, svommetur eller en styrketrening?\n" + 
		"Tast inn tall etter hva slags aktivitet som har blitt gjort");
		System.out.println("1: joggetur\n2: svommetur\n3: styrketrening");
		valg = leseInput.nextInt();
		
		switch (valg) {
		case 1: {
			joggeOkt jogg = new joggeOkt(true); //setter inn true hvis vil bruke hook eller ikke.
			jogg.startProsess();
			break;
		}
		case 2: {
			svommeOkt svom = new svommeOkt(false);
			svom.startProsess();
			break;
			}
		case 3: {
			styrkeTreningsOkt styrke = new styrkeTreningsOkt(false);
			styrke.startProsess();
			break;
			}
		}
		
		leseInput.close();
	}
}
